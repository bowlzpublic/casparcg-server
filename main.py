import os
import argparse

class CasparCGCleanUp:
    '''
    Class that houses the information needed to clean up the media..
    This happens if the directory changes while the scanner isn't running
    
    Solution... run this script before starting the CasparCG Server
    '''
    
    def __init__(self, main_dir) -> None:
        self.caspar_dir = main_dir
        print(f'Caspar Installation is: {self.caspar_dir}')
        self.media_folders = ['_media','pouch__all_dbs__']
        self.media_to_remove = self.collect_files()
    
    
    def collect_files(self) -> list[str]:
        """Run through the media directories and collect files that need to be deleted

        Returns:
            list[str]: list of files that should be deleted
        """        
        del_files = []
        for folder in self.media_folders:
            tempFolder = os.path.join(self.caspar_dir,folder)
            if os.path.exists(tempFolder):
                files = os.listdir(tempFolder)
                for file in files:
                    del_files.append(os.path.join(tempFolder,file))
            else:
                print(f'Media directory: {tempFolder}, not found... skipping')
        return del_files
    
    def run(self) -> None:
        """Run the process that will remove the media db files
        """     
        count = 0   
        for file in self.media_to_remove:
            os.remove(file)
            count += 1
        print(f'Removed {count} files')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Clean up CasparCG media db")
    parser.add_argument("--dir", dest="main_dir", 
                        default=r"C:\Users\jason\OneDrive\Documents\CasparCG_Server",
                        help="The installation directory for CasparCG")
    args = parser.parse_args()
    cg = CasparCGCleanUp(args.main_dir)
    cg.run()
    